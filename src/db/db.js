const mongoose = require('mongoose');
require('dotenv').config();
//const apiKey = process.env.MLA_API_KEY;


mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: true
});
